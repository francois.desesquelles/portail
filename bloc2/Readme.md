Bloc 2 - Algorithmique
======================

[DIU Enseigner l'informatique au lycée, Univ. Lille](../Readme.md)

**Intervenant·es** —
Laetitia Jourdan,
Francesco de Comité,
Philippe Marquet,
Patricia Everaere,
Jean-Stéphane Varré,
Benoit Papegay.

Premières activités sur les tris
================================

vendredi 8 janvier 2021

Travaux dirigés - Activité d'informatique sans ordinateur

* [Tris sans ordi](tri-sans-ordi/Readme.md)

mardi 12 janvier 2021

Travaux pratiques 

* [Reconnaître et programmer les tris](tris_anonymes/Readme.md)

Ressources sur les tris
-----------------------

* Présentation et animation des tris sur le site
  [lwh.free.fr/](http://lwh.free.fr/pages/algo/tri/tri.htm)
* Des algorithmes de tri visualisés avec des danses folkloriques sur la
  [chaîne vidéo AlgoRythmics](https://www.youtube.com/user/AlgoRythmics/videos)


Analyse des algorithmes (de tris)
=================================

mercredi 13 janvier 2021

Travaux pratiques 
* [Analyse en temps d'exécution des tris](analyse_tris/Readme.md)

Présentation
* _Analyse théorique des algorithmes, illustration sur les algorithmes
  de tri_  
  [Notebook Jupyter](analyse_tris/analyse_tris.ipynb)
  / également disponible sous forme d'un [fichier PDF](analyse_tris/analyse_tris.pdf)

jeudi 14 janvier 2021

Travaux dirigés
* _Analyse de complexité d'algorithmes_  
  [fichier PDF](tp/analyse_tris/exos_analyse_algos.pdf)
  / [fichier source Markdown](tp/analyse_tris/exos_analyse_algos.md)

Travaux pratiques 
* _Analyse de la complexité théorique des algorithmes en pratique
  grâce aux décorateurs_  
  Notebook Jupyter
  * [complexite_decorateurs.ipynb](analyse_tris/complexite_decorateurs.ipynb)
  * ou en ligne via le serveur `jupyter.fil.univ-lille1.fr`
    accessible à [frama.link/diu-ipynb-tris-decorateurs](https://frama.link/diu-ipynb-tris-decorateurs)
  * disponible sous forme d'un
	[fichier PDF](analyse_tris/complexite_decorateurs.pdf) ou d'un
	[source Python](analyse_tris/complexite_decorateurs.py)


Correction de programme
=======================

mercredi 30 juin 2021\
vendredi 2 juillet 2021

Présentations

Algorithmes gloutons
====================

mercredi 30 juin 2021\
vendredi 2 juillet 2021

Travaux dirigés
* [Introduction aux algorithmes gloutons](glouton/glouton-intro.md)
* [Exercices - algorithmes gloutons](glouton/glouton_diu.pdf) (fichier PDF)
    - [alice.py](glouton/alice.py)

Travaux pratiques
* [Le voyageur de commerce](tsp/Readme.md)

Algorithme des k plus proches voisins
=====================================

* [Ressources : cours, TD, TP, jeu de données...](knn/Readme.md) 
  - [cours](knn/cours_KNN.pdf)
  - [Travaux dirigés - knn](knn/exo-td-knn.md)
  - [Travaux pratiques - pokemon](knn/pokemon/readme.md) 

<!-- eof -->
