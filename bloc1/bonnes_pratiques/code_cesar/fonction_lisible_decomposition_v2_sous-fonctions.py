# + des identificateurs plus lisibles
# + une fonction paramétrée
# + séparation du calcul et de l'affichage : le calcul peut être réutilisé
# + quelques commentaire aident à la compréhension
# + de petites fonctions dont le code est facile à lire et donc à faire évoluer
# + la décomposition du code est lpus explicite, les commentaires dans le code deviennent superflus
# - des constantes numériques explicites
# - pas de documentation facilitant la compréhension et l'utilisation des fonctions définies
# - pas de test de validation du code

# vresion avec des "sous-fonctions", les paramètres des fonctions locales ne sont plus nécessaires (ici)

def est_lettre(car):
    return ord(car) >= 97 and ord(car) <= 122


def code_cesar_caractere(car, decalage = 1):
    
    def code_car():
        return chr ( 97 + (ord(car) - 97 + decalage) % 26 )
    
    if est_lettre(car):
        return code_car()
    else:
        return car


def code_mot(mot, decalage = 1):
    
    def code_cesar_mot():
        result = ''
        for lettre in mot :
            result = result + code_cesar_caractere(lettre, decalage)
        return result
    
    if len(mot) < 3:
        return mot
    else:
        return code_cesar_mot()
    

def code_phrase(phrase, decalage = 1):
    liste_mots = phrase.split()
    liste_code = []
    for mot in liste_mots :
        liste_code.append(code_mot(mot,decalage))    
    return ' '.join(liste_code)


def affiche_code_phrase(phrase, decalage = 1):    
    print( phrase + ' -> ' +   code_phrase(phrase, decalage) )
    

phrase = "Aujourd'hui, est le troisieme jour du DIU."
affiche_code_phrase(phrase, 13)    



