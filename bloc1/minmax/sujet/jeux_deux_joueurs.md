Les jeux à deux joueurs
=======================

Jeux bien connus,
[Othello](https://fr.wikipedia.org/wiki/Othello_%28jeu%29), les échecs,
les dames, [Tic-tac-toe](https://fr.wikipedia.org/wiki/Tic-tac-toe), le
[puissance 4](https://fr.wikipedia.org/wiki/Puissance_4), [le jeu de Nim](https://fr.wikipedia.org/wiki/Jeux_de_Nim)
et bien d'autres, ont pour point commun d'être des "*jeux à deux
joueurs au tour par tour*". Ils ont aussi en commun d'être des jeux *à
connaissance parfaite*, car, à tout moment, les deux joueurs possèdent
exactement la même connaissance de l'état du jeu. De plus ils ne font
pas intervenir le hasard. Ce sont ces jeux que nous allons étudier.

Ces jeux ont des points communs. Ainsi pour chacun, une partie est à
tout moment caractérisée par le *joueur courant* (le prochain qui doit
jouer) et par un état du jeu, que nous appellerons *situation courante*.
Il s'agit par exemple

-   de la configuration des pièces sur l'échiquier aux échecs,
-   du nombre d'allumettes restant à prendre dans le jeu de Nim
-   etc ...

De plus, ces jeux partagent un mécanisme de déroulement des parties
commun. Ce sont uniquement les *règles du jeu* différentes d'un jeu à
l'autre qui font la différence, pas la mécanique du jeu. Celle-ci se
base principalement sur le fait que les joueurs jouent alternativement
des "coups de jeu" en fonction de ce qui est autorisé par les règles
du jeu. Ces différents coups font évoluer le jeu de situation en
situation, jusqu'à ce que l'on atteigne une situation de fin de
partie. Le déroulement d'une partie peut donc être décrit ainsi :


**Déroulement des jeux à deux joueurs**


 1.  installer le jeu, c'est-à-dire **créer la *situation courante* initiale**    
 2.  déterminer le premier *joueur courant*  
 3.  tant que le **jeu n'est pas fini**  
	 + si le ***joueur courant* peut jouer** dans la *situation courante*   
		 + le ***joueur courant* joue**
           c'est-à-dire qu'il choisit un coup possible parmi les coups autorisés dans la *situation courante* du jeu. Chaque coup de jeu amène le jeu dans une nouvelle situation. Donc choisir un coup de jeu (= jouer) revient à choisir la prochaine *situation courante* parmi toutes les **situations suivantes possibles**.
		 + mettre à jour la *situation courante* suite au choix du joueur  
	 + sinon  
		 +  ne rien faire (la *situation courante* ne change pas)  
	 + l'autre joueur devient le *joueur courant*  
 4. `# le jeu est fini`    
    **afficher le résultat** (**le joueur vainqueur** ou partie nulle)

Ce mécanisme de jeu nous fournit un algorithme permettant de jouer aux jeux à deux joueurs qui nous concernent. 
Cet algorithme est le même pour tous ces jeux. Les variations sont dues uniquement aux règles du jeu.
Une analyse de cet algorithme permet d'identifier les fonctions qu'il est nécessaire de définir pour n'importe quel jeu à deux joueurs afin de pouvoir y jouer en suivant cette mécanique.

## Analyse

On identifie donc les fonctionnalités nécessaires 

 1.  à l'étape 1, déterminer comment installer le jeu
 2.  à l'étape 3, identifier quand le jeu se termine (c'est-à-dire quand la
     *situation courante* atteinte correspond à une fin de partie)
 3.  à l'étape 3, savoir si le joueur peut jouer
 4.  à l'étape 3, calculer quelles sont les prochaines situations suivantes
     possibles (ce sont celles parmi lesquelles le joueur doit
     choisir)
 5.  à l'étape 4, déterminer qui gagne ou pas

Grâce à une analyse descendante, on peut donc identifier pour chacun de ces points une fonction qu'il
faudra implémenter de manières différentes pour chacun des jeux que
l'on souhaite programmer. Dans l'ordre des points ci-dessus :

 1.  créer la situation initiale du jeu : `create_first_situation`
 2.  déterminer quand le jeu est fini, c'est-à-dire quand une situation est finale `is_game_finished`
 3.  déterminer si un joueur peut jouer `player_can_play`
 4.  calculer les situations filles `next_situations`
 5.  déterminer qui est vainqueur en fonction de la situation finale atteinte `get_winner`


On peut compléter ces fonctions par :

 -   une fonction pour afficher l'état du jeu, `display_game`
 -   une fonction pour permettre la saisie par le joueur humain de son coup de jeu, `human_player_plays`  
	cela permet de traduire l'étape *"le* joueur courant
     *joue"* quand le joueur est un humain, une interaction avec le joueur doit alors être gérée pour choisir la prochaine situation.

Enfin, à ces fonctions, il faut rajouter (par anticipation) une fonction d'évaluation, `eval_function` de la situation qui sera nécessaire pour le jeu à deux joueurs, pour que l'algorithme puisse donner une valeur aux situations de jeu.  


Le module <q><em>abstrait</em></q> [abstractgame.py](../code/abstractgame.py) propose une structure qui définit ces fonctions (si vous le jugez nécessaire, vous pouvez modifier les paramètres d'une fonction, mais il faudra alors la modifier de manière identique dans tous les jeux définis). Dans ce module, le plus souvent ces fonctions prennent en paramètre la situation courante du jeu, ainsi que le joueur courant (celui à qui c'est le tour de jouer). Selon les jeux, situation courante et joueur peuvent être représentés par des structures de données plus ou moins complexes (un simple entier, une chaîne de caractères, une tuple, un dictionnaire, etc.).

Pour chaque jeu à deux joueurs que l'on voudra implémenter, il faudra mettre en &oelig;uvre chacune de ces fonctions. 
Il sera également nécessaire de définir, en plus, pour chaque jeu, la structure de données permettant de représenter une situation du jeu, ainsi que d'éventuelles fonctions pour la manipulation de cette structure de données.
Par exemple, pour le jeu de Nim, une situation peut être simplement représentée par le nombre d'allumettes posées sur la table. La structure est évidemment plus complexe pour l'Othello ou le puissance 4.  

Chaque jeu peut être défini dans un module à part. Ces modules offrent donc les mêmes fonctions mais avec des implémentations différentes.



Dès que tous les jeux définissent ces fonctions, alors, si la programmation de l'algorithme de jeu ci-dessus s'appuie sur ces noms de fonctions, il sera "facile" de changer le jeu joué en modifiant le module de jeu importé.




<!--
Une des premières tâches que vous devez réaliser est donc cette analyse afin de produire **l'interface** des jeux à deux joueurs.

**Enveloppe de secours** 
Si vous êtes bloqué (après une réflexion suffisante) ou si souhaitez comparer votre analyse, nous vous proposons [cette aide](./aide/enveloppe_secours_analyse.md).
-->


## Exemple simple : le jeu de Nim

Les règles du jeu sont simples :

 > On dispose un tas d'allumettes au milieu de la table.    
 > Les (deux) joueurs ramassent tour à tour 2 ou 3 allumettes. Celui qui prend la dernière a gagné.    
 > S'il reste une seule allumette le jeu est nul.

Il existe des stratégies gagnantes pour ce jeu, mais sa simplicité en fait un très bon candidat pour tester l'algorithme de min-max.
La fonction d'évaluation est facile ici, à moins de prendre un nombre initial d'allumettes très grand, développer l'arbre de jeu à son terme et choisir le triplet *perdu*, *nul*, *gagné* convient.

### À faire

Recopiez le contenu de  `abstractgame.py` dans un fichier `jeu_de_nim.py`. Dans ce fichier programmez le jeu de Nim en produisant une définition, adaptée à ce jeu, de chacune des fonctions définies dans `abstractgame.py` (mise à part la fonction d'évaluation que vous pouvez pour le moment laisser de côté). Pour ce jeu, le code de ces fonctions sera probablement très simple. 

Dans le jeu de Nim, une situation de jeu correspond simplement au nombre d'allumettes qu'il reste sur la table. On peut donc, dans les fonctions ci-dessus, simplement représenter une situation du jeu de Nim par un entier représentant ce nombre d'allumettes.

### À faire

Programmez l'algorithme de jeu à deux joueurs présenté plus haut, les interactions avec le jeu se feront en utilisant les noms des fonctions définies dans `abstractgame.py`, et redéfinies dans `jeu_de_nim.py`.

Pour que ces fonctions soient définies, et correspondent aux règles du jeu de Nim, vous importerez `jeu_de_nim.py`.


### À faire

Testez votre programme en jouant au jeu de Nim dans une version où deux joueurs humains s'affrontent.

On peut aussi remplacer un des joueurs par un joueur qui prend aléatoirement  2 ou 3 allumettes. On peut intégrer ce joueur en gérant l'alternance des deux joueurs dans la partie <q>*le joueur courant joue*</q> du point 3 de l'algorithme ci-dessus.

### À faire

Codez l'algorithme min-Max qui permet de calculer une des *prochaines situations* autorisées dans une situation de jeu courant. L'exécution de cet algorithme doit donc être insérée au niveau de la partie *le joueur courant joue* du point 3 de l'algorithme ci-dessus, dans le cas où c'est au programme de jouer.

Vous aurez peut-être besoin des ressources suivantes&nbsp;:

 - [diaporama](../algo-minMax.odp) (format OpenDocument)
 - [l'algorithme du min-max](./algo_minmax.md)

Définissez une fonction d'évaluation pour le jeu de Nim : ici, la faible complexité du jeu permet de calculer jusqu'à la fin de la partie en choisissant une profondeur suffisante. Comme présenté en cours avec l'exemple du tic-tac-toe, une fonction d'évaluation pour laquelle les situations gagnantes reçoivent un score plus grand que les parties nulles, lui-même plus grand que le score des parties perdantes suffit.

Jouez contre votre programme.


### À faire

Codez pour les fonctions d'`abstractgame.py` pour un autre jeu, par exemple Othello ou le jeu du tic-tac-toe. Ici une situation de jeu doit être représentée par une grille 8x8 ou 3x3 selon le cas. La grille contient des pions des joueurs. À vous de choisir une structure de données adaptée.

Définissez également une fonction d'évaluation pour le jeu choisi et jouez contre votre programme.


**Nous vous conseillons de privilégier le jeu Othello**, pour lequel on peut plus facilement trouver des fonctions d'évaluation pertinentes.

* Il permet sans doute de mettre mieux en évidence que l'algorithme min-Max avec une bonne fonction d'évaluation joue de manière plus pertinente qu'un programme aléatoire.
* Il est également possible d'observer la différence de comportement de l'algorithme en faisant varier la profondeur de calcul de l'algorithme (essayez 1, 3 5 et 7 par exemple).
* Enfin, vous pouvez observer la variation de la qualité de jeu selon la fonction d'évaluation en en essayant plusieurs (voir ce qui a été évoqué en amphi et qui est proposé également dans [ce document](./aide/eval_othello.md).

