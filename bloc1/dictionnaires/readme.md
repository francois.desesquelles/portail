# Dictionnaires

Rapide introduction aux dictionnaires et aux exceptions

Notebook Jupyter

* [dictionnaires.ipynb](./dictionnaires.ipynb)
* également en ligne via le serveur `jupyter.fil.univ-lille1.fr`
  accessible à 
  [https://jupyter.fil.univ-lille1.fr/hub/.../dictionnaires.ipynb](https://jupyter.fil.univ-lille1.fr/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab-fil.univ-lille.fr%2Fdiu-eil-lil%2Fipynb&urlpath=tree%2Fipynb%2Fdictionnaires%2Fdictionnaires.ipynb&branch=master)

