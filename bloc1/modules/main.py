#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
	A little program using complex module. 
	
"""

import sys
import complex2 as complex

def usage():
    print('Usage: {:s} <x1> <y1> <x2> <y2>'.format(sys.argv[0]))
    print('\t<x1> <y1> : real and imaginary parts of a complex number')
    print('\t<x2> <y2> : real and imaginary parts of a second complex number')
    exit(1)


if len(sys.argv) != 5:
    usage()
    
try:
    x1 = float(sys.argv[1])
    y1 = float(sys.argv[2])
    x2 = float(sys.argv[3])
    y2 = float(sys.argv[4])
except ValueError:
    usage()
    

z1 = complex.create(x1, y1)
z2 = complex.create(x2, y2)

print()
print('z1 = ',end='')
complex.print(z1)
print ("z1's modulus = {:f}".format(complex.modulus (z1)))
print()

print('z2 = ',end='')
complex.print(z2)
print("z2's modulus = {:f}".format (complex.modulus (z2)))
print()

print('z1 + z2 = ', end='')
complex.print(complex.add(z1, z2))
print()

print('z1 * z2 = ', end='')
complex.print(complex.mul(z1, z2))
print()

exit(0)
