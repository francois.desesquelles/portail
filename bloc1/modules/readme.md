---
title: Programmation modulaire
date: juin 2019
author: Équipe pédagogique DIU EIL Lille
geometry: width=18cm, height=25cm
graphics: oui
numbersections: oui
---

## Objectifs

-   Comprendre l'intérêt de la programmation modulaire
-   Savoir concevoir et utiliser un module

# Pourquoi la programmation modulaire ?

-   développement logiciel (séparation des préoccupations)
-   modification et maintenance logicielle ("diviser pour régner")
-   réutilisabilité
-   création de "bibliothèque"/API, par exemple pour créer de nouveaux types de données  
  Une **API** (*application programming interface*) permet d'exposer/offrir des fonctionnalités. En ayant accès à la documentation d'une API, un utilisateur d'une API peut exploiter ces fonctionnalités sans avoir à connaître les détails de leur implémentation. Cette implémentation peut donc évoluer sans impact sur le code de l'utilisateur, tant que la spécification n'est pas modifiée.

# Les modules en Python

Module = fichier contenant un ensemble de déclarations (fonctions, variables, classes).

## Exemples de modules Python
Certains modules sont des modules "standard" livrés avec toute
distribution de Python, d'autres doivent être installés indépendamment
(par exemple avec `pip` ou `conda`, ou via le gestionnaire de module de Thonny).

Exemples de modules standard

* `math` : constantes et fonctions mathématiques
* `random` : fonctions de génération d'alea
* `turtle` : dessiner avec une tortue
* `sys` : fonctions système

Exemples de modules non standard

* `PIL` : package de traitement d'images
* `networkx` : graphes
* `pylab` : package scientifique. Regroupe `numpy` (calculs numériques
  et vectoriels) et `matplotlib` (création de graphiques)


## Importation d'un module

Importer un module permet de rendre disponible les définitions qu'il contient. Lors de l'importation d'un module l'ensemble des expressions qu'il contient sont interprétées.


Plusieurs formes d'importation sont possibles :

-   `from <module> import *`
-   `from <module> import <truc>`
-   `import <module>`
-   `import <module> as <autre nom>`

La différence entre la forme `from ... import ...` et la forme
`import ...` réside dans la syntaxe qui permet l'utilisation  des fonctionnalités
offertes par le module :

-   avec la première forme, il suffit d'invoquer le simple nom de la
    fonctionnalité ;
-   avec la seconde forme, il est nécessaire de *pleinement qualifier*
    le nom en le préfixant du nom du module. Le module crée alors un *espace de noms* qui regroupe les définitions réalisées dans le module.  
    Cette forme peut être à favoriser car elle permet d'éviter les conflits de nommage.

Voici quelques exemples classiques :

-   importation de toutes les fonctions définies dans un module (ici `random` et `math`), l'importation les rend disponibles :

    ```python
    >>> randint
      Traceback (most recent call last):
      File "<pyshell>", line 1, in <module>
      NameError: name 'randint' is not defined
    >>> from random import *
    >>> randint(0, 10)
    5
    >>> l = [1, 2, 3]
    >>> shuffle(l)
    >>> l
    [3, 1, 2]

    >>> from math import *
    >>> sin(pi)
    1.2246467991473532e-16
    ```

-   importation d'uniquement deux définitions du module `math`, les autres définitions du modules restent inconnues :

    ```python
    >>> from math import cos, pi
    >>> pi
    3.141592653589793
    >>> cos(pi)
    -1.0
    >>> sin(pi)
      Traceback (most recent call last):
      File "<pyshell>", line 1, in <module>
      NameError: name 'sin' is not defined
    ```

-   forme `import math`, les définitions importées sont placées dans l'espace de noms `math` et uniquement accessibles dans cet espace de noms :

    ```python    
    >>> import math
    >>> math.pi
    3.141592653589793
    >>> math.cos(math.pi)
    -1.0

    >>> pi
      Traceback (most recent call last):
      File "<pyshell>", line 1, in <module>
      NameError: name 'pi' is not defined
    ```

-   forme `import random as alea`, possibilité de choisir l'espace de noms lors de l'importation :

    ```python
    >>> import random as alea
    >>> alea.random(0, 10)
    8
    >>> l = [1, 2, 3]
    >>> alea.shuffle(l)
    >>> l
    [1, 3, 2]
    ```

En cas de conflit de nommage entre deux définitions de mêmes noms importées de deux modules différents, il est possible de lever l'ambiguïté en préfixant le nom de la définition à utiliser par le nom de son module (voir plus loin exemple avec `print`).


## Utiliser un module comme script

En Python, un module est un ensemble de déclarations faites
dans un fichier. Il est possible d'utiliser un module en tant que
script et l'exécuter.

Si ce module ne contient que des définitions de constantes et fonctions,
son exécution ne produit rien.

En revanche, s'il contient des instructions de calculs et/ou
d'impressions, ces calculs et/ou impressions sont effectués.

```bash
 $ python3 mon_module.py
```

C'est très bien lorsque le module est utilisé en tant que script
principal, mais cela peut être problématique lorsque ce module est importé par un autre script :
des calculs inutiles voire non souhaités peuvent être effectués et des
impressions parasites peuvent se produire.

Pour l'éviter, une solution consiste à placer  dans une instruction conditionnelle toutes les
instructions du module qui doivent être exécutées lorsqu'il est utilisé en tant que script. Le plus souvent cette instruction conditionnelle est placée à la fin du module. Elle est de la forme :

```python
if __name__ == '__main__':
   # instructions à exécuter en tant que script
```

La condition de cette instruction conditionnelle regarde si la variable
`__name__` vaut `'__main__'` ce qui est le cas uniquement si le module
est exécuté en tant que script principal. Cette condition est donc fausse lorsque le modlue est importé par un autre.


**Attention :** il y a deux caractères *blancs soulignés* (*underscore*) entourant les mots
`name` et `main`.

On peut par exemple profiter de cette possibilité pour gérer l'exécution des (doc)tests.

```python
if __name__ == '__main__':
   import doctest
   doctest.testmod()
```

Ainsi en phase de développement du module, il suffit d'exécuter le
module en tant que script principal pour effectuer ces tests. Bien
entendu, si le module est importé par un autre script, ces tests ne sont
pas exécutés.


## Exemple de conception de module : des cartes à jouer


Pour illustrer notre propos, nous souhaitons réaliser un (petit) module
pour représenter des cartes.


### Interface

Commençons par définir l'interface de ce module, c'est-à-dire
l'ensemble des fonctionnalités qu'il nous offre.

Chacune de ces fonctions peut être décrite par une spécification précisant

-   son nom
-   ses paramètres : leur type et leur rôle
-   la valeur qu'elle renvoie ou l'effet qu'elle produit
-   les contraintes d'utilisation (**UC** pour *Usage constraint*)
-   et éventuellement des exemples d'utilisation.

Ces informations indiquées dans la *docstring* de la fonction permettent de générer une documentation. Les exemples d'utilisation fournis sous la forme de *doctest* servent également de tests unitaires.

#### Le module `card`

Les fonctionnalités offertes par le module `card` sont

 - les valeurs et couleurs des cartes (`VALUES` et `COLORS`)
 - la création d'une carte définie par sa valeur et sa couleur (`create`)
 - l'accès aux données d'une carte (`get_value` et `get_color`)
 - la comparaison deux cartes entre elles (`compare`, `compare_values`, `compare_colors`, `equals`)
 - l'obtention d'une carte aléatoire (`random`)
 - une représentation d'une carte sous la forme d'une chaîne de caractères (`str`)
 - l'impression d'une carte (`print`)


> A propos du choix des noms de fonction :
> on peut se demander si la fonction de création ne devrait pas plutôt être nommée `create_card` au lieu de `create` qui peut sembler ambigüe.  
> Cependant, si l'on considère l'importation du module avec la forme recommandée `import card` , l'utilisation de cette fonction est préfixée du nom du module ce qui donne donc `card.create_card`, avec un sentiment de répétition que nous épargne `card.create`. D'où le choix réalisé de pas faire réapparaitre le terme *card* dans les noms de fonctions (`get_card_value`, `random_card`, ...).  
> *NB* : l'utilisation de classe d'objets rend caduque cette discussion.

Le module est spécifié par la [documentation](https://www.fil.univ-lille1.fr/~routier/diu/modules/doc/card.html).

#### utilisation du module

Cette documentation suffit à permettre l'utilisation de ce module par un autre programmeur.

Par exemple, le module `game.py` implémente une version très simplifiée du jeu de la bataille en utilisant la représentation des cartes proposés par le module `card`.

L'étude du [code source de `game.py`](./src/game.py) met en évidence l'exploitation du module `card` importé (ainsi que du module `random` de Python) et l'utilisation des définitions qu'il expose.

```python
import card
(...)
def create_deck():
    '''
    create and return a deck with all cards
    '''
    return [card.create(val, col) for col in card.COLORS for val in card.VALUES]
(...)
```
**L'écriture de ce code ne nécessite pas d'avoir connaissance de l'implémentation du module `card`.**

Ce module peut être utilisé comme script. Son "bloc main" est exécuté, pas celui du module `card`.
```bash
$ python3 game.py
```

#### implémentation du module `card`

L'étude du code d'implémentation du [module `card`](./src/card.py) permet de mettre en évidence un certain nombre de points. La [version `card_light.py`](./src/card_light.py) fournie permet de consulter le code sans les *docstrings*.

On peut ainsi observer :

 - l'importation renommée du module `random`
 - l'importation du module `builtins`
 - l'utilisation d'un dictionnaire pour représenter les cartes
 - l'utilisation des fonctions `get_color` et `get_value` pour accéder aux données sur les cartes et se détacher de l'implémentation
 - la fonction `compare` qui reprend la convention classique pour son résultat
    - &lt; 0 si la première valeur est considérée plus petite que la seconde
    - &gt; 0 si la première valeur est considérée plus grande que la seconde
    - = 0 si les deux valeurs sont considérées égales
 - la définition d'une fonction `print` qui utilise la fonction `print` prédéfinie de Python pour laquelle il est nécessaire de lever le conflit de nommage à l'aide de `builtins.print`
 - (dans `card.py`) la possibilité de faire un `import` local, ici le  `import doctest` dans le "bloc main"
 - le "bloc main'' qui n'est pas exécuté par le script `game.py`

#### autre implémentation

Pour insister et mettre en évidence la séparation entre l'interface d'un module et son implémentation, une seconde version du module de gestion des cartes a été mise en &oelig;uvre dans le fichier [`card_tuple.py`](./src/card_tuple.py).

Cette version alternative peut facilement être utilisée au sein du module `game.py`. Il suffit de remplacer l'importation du module `card` par

```python
import card_tuple as card
```

La seconde implémentation étant elle aussi conforme à la spécification du module, on peut alors vérifier que le programme se comporte exactement de la même manière qu'avec la première version du module `card`.

En consultant le code de cette seconde version, on peut noter que le changement se situe au niveau du choix de la structure de donnée pour représenter les cartes. Cette fois les cartes sont représentées pas un tuple *(couleur, hauteur)*.

On constate que cette modification n'impacte que les fonctions `create`, `get_color` et `get_value` qui permettent d'abstraire le reste du code de l'implémentation.
