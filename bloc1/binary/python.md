# Rappels (compléments...) Python

> Ce document présente
> 
> * les écritures binaire, octale, et hexadémimale des entiers en Python
> * les opérations logiques sur les entiers, opérations bit à bit

## Entiers littéraux

Python permet différentes écritures des nombres entiers : 

* écriture usuelle décimale
* écriture en binaire en préfixant par les littéraux par `0b`
  
    ```python
	>>> 0b10100
	20
	>>> -0b11 * 0b10100
	-60
    ```

* écriture en octal en préfixant par les littéraux par `0o`
  
    ```python
	>>> 0o24
	20
	>>> -0o3 * 0o24
	-60
	```
	
* écriture en hexadécimal en préfixant par les littéraux par `0x`
  
    ```python
	>>> 0x14
	20
	>>> -0x3 * 0x14
	-60
	```
	
## Opérations logiques sur les entiers

Python dispose d'opérateurs logiques sur les entiers : les opérations booléennes classiques sont étendues aux bits de l'écriture binaire des entiers, avec la convention que le bit `0` correspond à la valeur booléenne `False`, et le bit `1` à `True`.

* opérateur **et** : 

	```python
	>>> 131 & 19
	3
	>>> 0b10000011 & 0b10011
	3
	```
	
* opérateur **ou** :

	```python
	>>> 131 | 19
	147
	>>> 0b10000011 | 0b10011
	147
	```

* opérateur **ou exclusif** : 

	```python
	>>> 131 ^ 19
	144
	>>> 0b10000011 ^ 0b10011
	144
	```

En plus de ces opérations logiques, Python propose deux opérateurs de décalage

* **Décalage à gauche** : 

	```python
	>>> 131 << 1
	262
	>>> 131 << 2
	524
    >>> 0b101 << 2 == 0b10100
    True
    ```

* **Décalage à droite** : 

	```python
	>>> 131 >> 1
	65
	>>> 131 >> 2
	32
    >>> 0b101 >> 1 == 0b10 
    True
	```

<!-- eof -->
