# Manipulation des outils ping et traceroute

L'objectif de cette partie est de manipuler les outils ping et traceroute

à partir de votre machine virtuelle, vous allez vérifier que les machines distantes suivantes sont accesibles. Pour cela, vous utiliserez la commande ping. Pour chaque machine, vous observez si elle répond et si oui regarder le temps nécessaire. 

* www.univ-lille.fr
* www.polytech-lille.fr
* www.google.fr
* www.cmu.edu
* www.iuj.ac.jp

Si certaines machines ne répondent pas, essayez tout de même d'y accéder avec votre navigateur web. Que remarquez-vous ? Pourquoi ?

Vous allez maintenant recommencer l'expérimentation mais à partir d'un serveur de l'université. Pour cela, vous devez vous connecter en ssh à l'adresse 134.206.90.80 en utilisant le login pifou et le mot de passe noté sur le tableau.

Quelles sont les différences ? Pourquoi ?

Toujours à partir de la machine 134.206.90.80, utilisez la commande traceroute à destination des mêmes machines.

Que pouvez-vous dire sur le chemin utilisé ?
